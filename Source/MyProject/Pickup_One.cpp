// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup_One.h"

// Sets default values
APickup_One::APickup_One()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APickup_One::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickup_One::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

